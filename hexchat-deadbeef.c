#define PNAME "DeaDBeeF"
#define PDESC "/np says current track in a channel"
#define PVERSION "0.1"


#include <limits.h>
#include <stdio.h>
#include <hexchat-plugin.h>


static hexchat_plugin *ph;


static int np_cb(char *word[], char *word_eol[], void *userdata)
{
    FILE *db_out = popen("deadbeef --nowplaying '%a - %t'", "r");

    if (!db_out) {
        hexchat_print(ph, PNAME ": cannot open process");
        return HEXCHAT_EAT_ALL;
    }


    char np_str[LINE_MAX];

    if (fgets(np_str, LINE_MAX, db_out))
        hexchat_commandf(ph, "ME np: %s", np_str);
    else
        hexchat_print(ph, PNAME ": cannot read line");


    pclose(db_out);

    return HEXCHAT_EAT_ALL;
}


int hexchat_plugin_init(hexchat_plugin * plugin_handle,
                        char **plugin_name, char **plugin_desc,
                        char **plugin_version, char *arg)
{
    ph = plugin_handle;

    *plugin_name = PNAME;
    *plugin_desc = PDESC;
    *plugin_version = PVERSION;

    hexchat_hook_command(ph, "np", HEXCHAT_PRI_NORM, np_cb,
                         "Usage: NP, " PDESC, NULL);

    hexchat_print(ph, PNAME " plugin loaded");

    return 1;
}
