PROJECT = hexchat-deadbeef

$(PROJECT).so: $(PROJECT).c Makefile
	gcc -Wl,--export-dynamic -march=native -O2 -shared -fPIC -W -Wall $< -o $@
